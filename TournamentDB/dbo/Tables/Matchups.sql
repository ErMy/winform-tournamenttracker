﻿CREATE TABLE [dbo].[Matchups]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [WinnerId] INT NOT NULL, 
    [MatchupRound] INT NOT NULL, 
    CONSTRAINT [FK_WinnerId_Teams] FOREIGN KEY ([WinnerId]) REFERENCES [Teams]([Id])
)
