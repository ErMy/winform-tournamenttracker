﻿CREATE TABLE [dbo].[MatchupEntries]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [MatchupId] INT NOT NULL, 
    [ParentMatchupId] INT NOT NULL, 
    [TeamCompetingId] INT NOT NULL, 
    [Score] REAL NOT NULL, 
    CONSTRAINT [FK_MatchupId_Matchups] FOREIGN KEY ([MatchupId]) REFERENCES [Matchups]([Id]), 
    CONSTRAINT [FK_TeamCompetingId_Teams] FOREIGN KEY ([TeamCompetingId]) REFERENCES [Teams]([Id]), 
    CONSTRAINT [FK_ParentMatchupId_Matchups] FOREIGN KEY ([ParentMatchupId]) REFERENCES [Matchups]([Id])
)
