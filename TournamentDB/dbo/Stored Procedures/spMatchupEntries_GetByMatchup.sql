﻿CREATE PROCEDURE [dbo].[spMatchupEntries_GetByMatchup]
	@MatchupId int

AS
BEGIN
	SET NOCOUNT ON;

	select e.*
	from dbo.MatchupEntries e
	where e.MatchupId = @MatchupId;

END
