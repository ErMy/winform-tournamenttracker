﻿CREATE PROCEDURE [dbo].[spMatchups_GetByTournament]
	@TournamentId int
AS
BEGIN
	SET NOCOUNT ON;
	
	select ma.*
	from dbo.Matchups as ma
	inner join dbo.MatchupEntries as me
	on ma.Id = me.MatchupId
	inner join dbo.Teams as t
	on t.Id = me.TeamCompetingId
	inner join dbo.TournamentEntries as te
	on t.Id = te.TeamId
	where te.TournamentId = @TournamentId;


END
