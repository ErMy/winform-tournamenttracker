﻿CREATE PROCEDURE [dbo].[spTeamMembers_GetByTeam]
	@TeamId int
AS
BEGIN
	SET NOCOUNT ON;

	select tm.*
	from dbo.TeamMembers tm
	inner join dbo.Teams t
	on t.Id = tm.TeamId
	where t.Id = @TeamId;

END
