﻿CREATE PROCEDURE [dbo].[spTeam_GetByTournament]
	@TournamentID int

AS
BEGIN
	SET NOCOUNT ON;

	select t.*
	from dbo.Teams t
	inner join dbo.TournamentEntries e
	on t.Id = e.TeamId
	where e.TournamentId = @TournamentID;

END